import { Component } from '@angular/core';
import { NavigationLink } from './models/navigation-link';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public navigationLinks: NavigationLink[] = [
    { index: 0, label: 'Inicio', route: '/home', icon: 'home' },
    { index: 1, label: 'Gráfica', route: '/chart', icon: 'assessment' },
    { index: 2, label: 'Contraseña', route: '/password', icon: 'admin_panel_settings' },
  ];
  public hiddenStickyBar = false;
  public sticky = false;
  public readonly welcome = `¡Esta barra se muesta dinámicamente con el scroll!  <span>&#11088;</span>`;

  private currentScrollTopValue = 0;

  public onRouterContainerScroll(event: any): void {
    console.log(event.target.scrollTop);
    const scrollUp = this.currentScrollTopValue > event.target.scrollTop;
    this.sticky = event.target.scrollTop > 0;

    if (scrollUp) {
      this.hiddenStickyBar = false;
    } else if (!scrollUp && this.sticky) {
      this.hiddenStickyBar = true;
    }
    this.currentScrollTopValue = event.target.scrollTop;
  }
}
