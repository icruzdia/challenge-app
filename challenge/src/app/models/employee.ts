import { EmployeeEvent } from './event';

export interface Employee {
  id: string;
  firstName: string;
  lastName: string;
  manager: string;
  events: EmployeeEvent[];
}
