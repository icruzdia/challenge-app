import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable()
export class EarthQuakeService {
  private earthQuake$: BehaviorSubject<string>;
  private earthQuakenumber = 2;
  private refreshIntervalId;

  /* Simulate the data stream*/
  public getEarthQuakeData(): Observable<string> {
    this.earthQuake$ = new BehaviorSubject<string>(
      'Terremoto ' + this.earthQuakenumber
    );
    this.refreshIntervalId = setInterval(() => {
      this.earthQuakenumber++;
      this.earthQuake$.next('Terremoto ' + this.earthQuakenumber);
    }, 1000);
    return this.earthQuake$;
  }

  public stopDataStream(): void {
    clearInterval(this.refreshIntervalId);
    this.earthQuakenumber = 2;
    this.earthQuake$.unsubscribe();
  }
}
