import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { employees } from 'src/app/constants/employee-array';
import { Employee } from 'src/app/models/employee';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public displayedColumns: string[] = [
    'id',
    'firstName',
    'lastName',
    'manager',
  ];
  public dataSource: MatTableDataSource<Employee>;
  public homeDataLabel = 'Información de empleados';

  private employees: Employee[];
  private constEmployee = employees;

  ngAfterViewInit(): void {
    this.employees = [];
    this.dataSource = new MatTableDataSource<Employee>(this.employees);
    this.dataSource.paginator = this.paginator;
    for (let index = 0; index < 1000; index++) {
      this.employees = this.employees.concat(this.constEmployee);
      this.dataSource.data = this.employees;
    }
  }
}
