export interface NavigationLink {
  index: number;
  label: string;
  route: string;
  icon: string;
}
