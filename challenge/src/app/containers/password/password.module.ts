import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PasswordRoutingModule } from './password-routing.module';
import { PasswordComponent } from './password.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [PasswordComponent],
  imports: [CommonModule, PasswordRoutingModule, MatCardModule],
  exports: [PasswordComponent],
})
export class PasswordModule {}
