import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    loadChildren: () =>
      import('./containers/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'chart',
    loadChildren: () =>
      import('./containers/chart/chart.module').then((m) => m.ChartModule),
  },
  {
    path: 'password',
    loadChildren: () =>
      import('./containers/password/password.module').then(
        (m) => m.PasswordModule
      ),
  },
  {
    path: '**',
    loadChildren: () =>
      import('./containers/not-found/not-found.module').then(
        (m) => m.NotFoundModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
