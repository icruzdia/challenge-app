import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { ChartsModule } from 'ng2-charts';
import { EarthQuakeService } from 'src/app/services/earthquake-data.service';
import { ChartRoutingModule } from './chart-routing.module';
import { ChartComponent } from './chart.component';

@NgModule({
  declarations: [ChartComponent],
  imports: [CommonModule, ChartRoutingModule, MatCardModule, ChartsModule],
  providers: [EarthQuakeService],
  exports: [ChartComponent],
})
export class ChartModule {}
