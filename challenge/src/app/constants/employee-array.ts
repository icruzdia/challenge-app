import { Employee } from '../models/employee';

export const employees: Employee[] = [
  {
    id: '1',
    firstName: 'Pablo',
    lastName: 'Caselas',
    manager: 'Caselas',
    events: [
      { from: '2020-08-06', to: '2020-08-07', type: 'vacation' },
      { from: '2020-08-20', to: '2020-08-23', type: 'maternity_leave' },
    ],
  },
  {
    id: '2',
    firstName: 'Juan',
    lastName: 'Rodríguez',
    manager: 'Caselas',
    events: [
      { from: '2020-08-06', to: '2020-08-08', type: 'sickness_leave' },
      { from: '2020-08-20', to: '2020-08-23', type: 'sickness_leave' },
    ],
  },
  {
    id: '3',
    firstName: 'Pedro',
    lastName: 'Gutiérrez',
    manager: 'Caselas',
    events: [
      { from: '2020-08-06', to: '2020-08-08', type: 'sickness_leave' },
      { from: '2020-08-20', to: '2020-08-23', type: 'sickness_leave' },
    ],
  },
  {
    id: '4',
    firstName: 'Sergey',
    lastName: 'Brin',
    manager: 'Cook',
    events: [
      { from: '2020-08-06', to: '2020-08-07', type: 'vacation' },
      { from: '2020-08-20', to: '2020-08-23', type: 'maternity_leave' },
    ],
  },
  {
    id: '5',
    firstName: 'Rod',
    lastName: 'Stewart',
    manager: 'Cook',
    events: [
      { from: '2020-08-06', to: '2020-08-08', type: 'sickness_leave' },
      { from: '2020-08-20', to: '2020-08-23', type: 'sickness_leave' },
    ],
  },
  {
    id: '6',
    firstName: 'Dontaye',
    lastName: 'Draper',
    manager: 'Cook',
    events: [
      { from: '2020-08-06', to: '2020-08-08', type: 'sickness_leave' },
      { from: '2020-08-20', to: '2020-08-23', type: 'sickness_leave' },
    ],
  },
];
