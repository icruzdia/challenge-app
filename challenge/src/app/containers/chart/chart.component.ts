import { Component, OnDestroy, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { EarthQuakeService } from 'src/app/services/earthquake-data.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit, OnDestroy {
  public earthquakeDataLabel = 'Datos en caso de un terremoto';
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['Terremoto 1'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartData: ChartDataSets[] = [
    { data: [10], label: 'Escala sismológica de Richter' },
  ];
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(123,31,162,0.7)',
    },
  ];

  constructor(private earthQuakeService: EarthQuakeService) {}

  ngOnInit(): void {
    this.earthQuakeService
      .getEarthQuakeData()
      .subscribe((earthQuakeData: string) => {
        this.barChartData[0].data.push(this.getRndNumber());
        this.barChartLabels.push(earthQuakeData);
      });
  }

  ngOnDestroy(): void {
    this.earthQuakeService.stopDataStream();
  }

  private getRndNumber(): number {
    return +Math.floor(Math.random() * 10).toFixed(0);
  }
}
