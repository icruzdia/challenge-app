export interface EmployeeEvent {
  from: string;
  to: string;
  type: string;
}
