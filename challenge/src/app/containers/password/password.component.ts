import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
  public pwdDataLabel = 'Comprobación de contraseñas más comunes';

  constructor() { }

  ngOnInit(): void {
  }

}
